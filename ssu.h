#ifndef _H_SSU
#define _H_SSU

#define FILE_MODE 0
#define CMD_MODE  1

typedef struct {
  const char* path;
  unsigned int length;
  unsigned int mode;
} Module;

#endif
