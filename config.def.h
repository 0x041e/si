#include "ssu.h"

int CYCLE_TIME = 2;

/**
 *[<brightness>][<Disc/Char/Full>@<Batt %>][<Weekday><Month(abbrv)><Day>}{<time> (<timezone>)]
 */
static const Module Modules[] = {
  {"[", 0},
  {"xbacklight -get | sed 's/[.].*//'", 3+1, CMD_MODE}, 
  {"][", 0},
  {"/sys/class/power_supply/BAT1/status", 4+1, FILE_MODE},
  {"@", 0},
  {"/sys/class/power_supply/BAT1/capacity", 3+1, FILE_MODE},
  {"]", 0},
  {"date +\"[\%a \%b \%d}{\%R (\%z)]\"", 256, CMD_MODE}
};
