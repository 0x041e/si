#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include "ssu.h"
#include "config.h"

#define LENGTH(X) (sizeof X / sizeof X[0])
#define MAX_STAT 1024

char*
lread(const char* loc, unsigned int n, int mode) {
  char* str = malloc(sizeof(char) * n);
  FILE* f; 
  f = (mode == FILE_MODE) ? fopen(loc, "r") : popen(loc, "r");
  assert(f != NULL); 
  fgets(str, n, f); 
  str[strcspn(str, "\n")] = '\0'; // fix 'short' buffers
  fclose(f); // pclose is just an alias for fclose
  return str;
}

int
main(void) {
  while(1) {
    char status[MAX_STAT] = "xsetroot -name \"";

    // apply modules //
    for (unsigned int i = 0; i < LENGTH(Modules); i++) {  
      char* new;
      if (Modules[i].length == 0) {
        new = (char*)Modules[i].path;
        strcat(status, new);
      } else {
        new = lread(Modules[i].path, Modules[i].length, Modules[i].mode);
        strcat(status, new);
        free(new);
      }
    }

    // end status //
    strcat(status, "\"");
    system(status);
    sleep(CYCLE_TIME);
  }
  return 0;
}
